<?php

namespace App\Repositories\Blog;

use App\Repositories\BaseRepository;

class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    /**
     * Get the desired model
     */
    public function getModel()
    {
        return \App\Models\Blog::class;
    }

    /**
     * Get the desired product
     */
    public function getProduct()
    {
        return "abc";
    }
}
