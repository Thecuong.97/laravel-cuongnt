<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogResource;
use Illuminate\Http\Request;
use App\Repositories\Blog\BlogRepositoryInterface;
use App\Models\Blog;

class BlogController extends Controller
{
    /**
     * @var BlogRepositoryInterface|\App\Repositories\Repository
     */
    protected $blogRepo;
    
    public function __construct(BlogRepositoryInterface $blogRepo)
    {
        $this->blogRepo = $blogRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources
     */
    public function index()
    {
        $blogs = $this->blogRepo->getAll();

        return BlogResource::collection($blogs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Resources
     */
    public function show($id)
    {
        $blogs = Blog::findOrFail($id);

        return new BlogResource($blogs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\BlogRequest $request
     * @return \Illuminate\Http\Resources
     */
    public function store(BlogRequest $request)
    {

        $data = $request->all();

        $blogs = $this->blogRepo->create($data);

        return new BlogResource($blogs);
    }

    /**
     *  Update the specified resource in storage.
     *
     * @param  App\Http\Requests\BlogRequest $request
     * @return \Illuminate\Http\Resources
     */
    public function update(BlogRequest $request, $id)
    {
        $data = $request->all();
        $blogs = $this->blogRepo->update($id, $data);

        return new BlogResource($blogs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Resources
     */
    public function delete(Request $request, Blog $id)
    {
        $id->delete($request->all());
        return response()->json(null, 204);
    }
}
